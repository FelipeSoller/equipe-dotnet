# Metatron

## Desafio

Desenvolver o Frontend de uma aplicação de TODO list em React.

Criar um README em seu repositório explicando como você construiu sua aplicação, quais tecnologias foram utilizadas, quais foram as dificuldades, porque escolheu uma determinada biblioteca.

## Funcionalidades

### TODO List

- Adicionar tarefas;
- Concluir tarefas;
- Editar tarefas;
- Excluir tarefas;

## Desenvolvimento

### Tecnologias Utilizadas

- ReactJS
- JavaScript
- HTML5
- CSS3
- UUID - Pacote para criação de ID's aleatórios com mínima possibilidade de repetição.

### Passo a Passo do desenvolvimento

- Criação do aplicativo React no Terminal;
- Criação do Componente TodoForm;
- Criação do Componente TodoList;
- Criação do Componente Todo;
- Estilização do React App.

### Maiores desafios

Neste projeto, meu maior desafio foi desenvolver o aplicativo com a utilização dos Hooks do React.JS.

Um Hook é uma função especial que te permite utilizar recursos do React. Para este projeto, foi utilizado o useState, que é um Hook que te permite adicionar o state do React a um componente de função e o useEffect, que é um Hook que indica ao React que o componente precisa fazer algo apenas depois da renderização

### Demonstração do App

Acesse o link abaixo para demonstração do App desenvolvido.
https://metatron-todoapp.netlify.app/